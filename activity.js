// db.courses.insertMany([
//     {
//     "name": "HTML Basics",
//     "price": 20000,
//     "isActive": true,
//     "instructor": "Sir Alvin"
//     },
//     {
//     "name": "CSS 101 + Flexbox",
//     "price": 21000,
//     "isActive": true,
//     "instructor": "Sir Alvin"
//     },
//     {
//     "name": "Javascript 101",
//     "price": 32000,
//     "isActive": true,
//     "instructor": "Ma'am Tine"
//     },
//     {
//     "name": "Git 101, IDE and CLI",
//     "price": 19000,
//     "isActive": false,
//     "instructor": "Ma'am Tine"
//     },
//     {
//     "name": "React.Js 101",
//     "price": 25000,
//     "isActive": true,
//     "instructor": "Ma'am Miah"
//     },
// ])

// Sir Alvin & gte 20000
// db.courses.find({$and:[{instructor:{$regex: 'alvin',$options: '$i'}},{price:{$gte:20000}}]},{_id:0,name:1,price:1})

// Ma'am Tine & inactive
// db.courses.find({$and:[{instructor:{$regex: 'tine',$options: '$i'}},{isActive:false}]},{_id:0,name:1,price:1})

// Course with 'r' in Name, lte 25000 
// db.courses.find({$and:[{name:{$regex: 'r',$options: '$i'}},{price:{$lte:25000}}]})

// Update course of course with price lt 2100 to inactive
// db.courses.updateMany({price:{$lt:21000}},{$set:{isActive:false}})

// Delete courses gte 25000
// db.courses.deleteMany({price:{$gt:25000}})