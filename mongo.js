// db.products.insertMany([
//     {
//       "name": "iPhone X",
//       "price": 30000,
//       "isActive": true
//     },
//     {
//       "name": "Samsung Galaxy S21",
//       "price": 51000,
//       "isActive": true
//     },
//     {
//       "name": "Razer Blackshark V2X",
//       "price": 2800,
//       "isActive": false
//     },
//     {
//       "name": "RAKK Gaming Mouse",
//       "price": 1800,
//       "isActive": true
//     },
//     {
//       "name": "Razer Mechanical Keyboard",
//       "price": 4000,
//       "isActive": true
//     },
// ])
    
// Query Operators
// Allows us to expand our queries and define conditions instead of just looking
// for specific values
    
// $gt, $lt,$gte,$lte
// $gt - query operator which means greater than
// db.products.find({price:{$gt: 3000}})
// 
// $lt - query operator which means less than
// db.products.find({price:{$lt:3000}})
// 
// $gte - query operator which means greater than or equal
// db.products.find({price:{$gte:30000}})
//
// $lte - query operator which means less than or equal
// db.products.find({price:{$lte:2800}})

// db.users.insertMany([
//    {
//    "firstname": "Mary Jane",
//    "lastname": "Watson",
//    "email": "mjtiger@gmail.com",
//    "password": "tigerjackpot15",
//    "isAdmin": false
//    },
//    {
//    "firstname": "Gwen",
//    "lastname": "Stacy",
//    "email": "stacyTech@gmail.com",
//    "password": "stacyTech1991",
//    "isAdmin": true
//    },
//    {
//    "firstname": "Peter",
//    "lastname": "Parker",
//    "email": "peterWebDevgmail.com",
//    "password": "webdeveloperPeter",
//    "isAdmin": true
//    },
//    {
//    "firstname": "Jonah",
//    "lastname": "Jameson",
//    "email": "jjjameson@gmail.com",
//    "password": "spideyismenace",
//    "isAdmin": false
//    },
//    {
//    "firstname": "Otto",
//    "lastname": "Octavius",
//    "email": "ottoOctopi@gmail.com",
//    "password": "doc0ck15",
//    "isAdmin": true
//    },
// ])

// $regex - query operator which will allow us to find documents which
// will match the characters/pattern of the characters we are looking for

// $regex looks for documents with partial match and by default in case
// case sensitive
// db.users.find({firstname:{$regex: 'O'}})
 
// $options - used our regex will be case insensitive
// db.users.find({firstname:{$regex: 'O',$options: '$i'}})

// You can also find for documents with partial matches
// db.products.find({name:{$regex: 'phone', $options: '$i'}})

// Find users whose email have the word 'web' in it
// db.users.find({email:{$regex: 'web', $options: '$i'}})

// using $regex look for products which name has the word 'razer'
// db.products.find({name:{$regex: 'razer', $options: '$i'}})
// using $regex look for products which name has the word 'rakk'
// db.products.find({name:{$regex: 'rakk', $options: '$i'}})

// $or $and - logical operators - works almost the same way 
// db.products.find({$or:[{name:{$regex: 'x', $options: '$i'}},{price:{$lte:10000}}]})
// db.products.find({$or:[{name:{$regex: 'x', $options: '$i'}},{price:{$gte:30000}}]})
 
// $and - look or find for document that satisfies both conditions

// db.products.find({$and:[{name:{$regex: 'razer',$options: '$i'}},{price:{$gte:3000}}]})
// db.products.find({$and:[{name:{$regex: 'x',$options: '$i'}},{price:{$gte:3000}}]})
// db.users.find({$and:[{lastname:{$regex: 'w',$options: '$i'}},{isAdmin:false}]})

// Field Projection - allows us to show/hide certain properties/fields
// db.collection.find({query},{projection}) - 0 means hide, 1 means show
// db.users.find({},{_id:0,password:0})

// db.users.find({isAdmin:true},{_id:0,email:1})
 
// _id field must be explicitly hidden if you want to hide it.
// We can also just pick out which fields to show
// db.users.find({isAdmin:false},{firstname:1,lastname:1})

// db.products.find({price:{$gte:10000}},{_id:0,name:1,price:1})

// db.courses.insertMany([
//     {
//     "name": "HTML Basics",
//     "price": 20000,
//     "isActive": true,
//     "instructor": "Sir Alvin"
//     },
//     {
//     "name": "CSS 101 + Flexbox",
//     "price": 21000,
//     "isActive": true,
//     "instructor": "Sir Alvin"
//     },
//     {
//     "name": "Javascript 101",
//     "price": 32000,
//     "isActive": true,
//     "instructor": "Ma'am Tine"
//     },
//     {
//     "name": "Git 101, IDE and CLI",
//     "price": 19000,
//     "isActive": false,
//     "instructor": "Ma'am Tine"
//     },
//     {
//     "name": "React.Js 101",
//     "price": 25000,
//     "isActive": true,
//     "instructor": "Ma'am Miah"
//     },
// ])

// Sir Alvin & gte 20000
// db.courses.find({$and:[{instructor:{$regex: 'alvin',$options: '$i'}},{price:{$gte:20000}}]},{_id:0,name:1,price:1})

// Ma'am Tine & inactive
// db.courses.find({$and:[{instructor:{$regex: 'tine',$options: '$i'}},{isActive:false}]},{_id:0,name:1,price:1})

// Course with 'r' in Name, lte 25000 
// db.courses.find({$and:[{name:{$regex: 'r',$options: '$i'}},{price:{$lte:25000}}]})

// Update course of course with price lt 2100 to inactive
// db.courses.updateMany({price:{$lt:21000}},{$set:{isActive:false}})

// Delete courses gte 25000
// db.courses.deleteMany({price:{$gt:25000}})